import discord
from discord.flags import MemberCacheFlags
from discord.utils import get
from discord.ext import commands
from discord import Embed
from passwords import get_pass

token=f"{get_pass('token')}"
intents = discord.Intents.all()
intents.members = True
bot = commands.Bot(command_prefix=commands.when_mentioned_or("02 ","zt ","ZT ","Zt ","zT ","02","zt","ZT","Zt","zT"), case_insensitive=True, guild_subscriptions=True, intents=intents)
bot.remove_command('help')
    
@bot.event
async def on_ready():
    print("02_ONLINE!")
    await bot.change_presence(status=discord.Status.online, activity=discord.Game(name="with my darlin'"))

@bot.command(name="good", aliases=["GM","Good_mornin","GOOD_MORNING",]) 
async def Good(ctx):
    await ctx.reply(f"Ohio darlin'")

@bot.command(name='ping')
async def ping(ctx):
    await ctx.send(f'Pong! {round(bot.latency*1000, 1)} ms')

bot.run(token)
